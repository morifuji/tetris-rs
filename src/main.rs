#[macro_use]
extern crate termion;

extern crate log;

use std::io::Write;
use std::io::{stdin, stdout};
use std::sync::{Arc, Mutex};
use std::thread;
use termion::event::{Event, Key};
use termion::input::{MouseTerminal, TermRead};
use termion::raw::IntoRawMode;

pub mod field;
pub mod mino;
pub mod score;

pub const COLUMN_SIZE: usize = 10;
pub const ROW_SIZE: usize = 9;

pub const FIELD_PADDING_X: u16 = 5;
pub const FIELD_PADDING_Y: u16 = 5;

pub const SCORE_PADDING_Y: u16 = 2;

const DURATION: u32 = 500;

pub type StdoutType = MouseTerminal<termion::raw::RawTerminal<std::io::Stdout>>;

fn main() {
    let mut stdout = MouseTerminal::from(stdout().into_raw_mode().unwrap());

    run(&mut stdout);
    write!(
        stdout,
        "{}{}GAME OVER{}",
        termion::clear::All,
        termion::cursor::Goto(
            FIELD_PADDING_X + (COLUMN_SIZE.div_euclid(2) as u16),
            FIELD_PADDING_Y + (ROW_SIZE.div_euclid(2) as u16)
        ),
        termion::cursor::Goto(0, FIELD_PADDING_Y * 2 + (ROW_SIZE as u16))
    )
    .unwrap();
}

fn run(stdout: &mut StdoutType) {
    let mut field = field::Field::new();
    field.display(stdout);

    let field_shared = Arc::new(Mutex::new(field));

    let field_shared2 = Arc::clone(&field_shared);
    let handle_input = thread::spawn(move || {
        let stdin = stdin();

        for c in stdin.events() {
            let evt = c.unwrap();
            match evt {
                Event::Key(Key::Char('q')) => break,
                Event::Key(Key::Down) => {
                    field_shared2.lock().unwrap().move_down();
                }
                Event::Key(Key::Left) => {
                    field_shared2.lock().unwrap().move_left();
                }
                Event::Key(Key::Right) => {
                    field_shared2.lock().unwrap().move_right();
                }
                Event::Key(Key::Up) => {
                    field_shared2.lock().unwrap().force_drop();
                }
                Event::Key(Key::Char('l')) => {
                    field_shared2.lock().unwrap().lotate();
                }
                Event::Key(Key::Char('d')) => {
                    field_shared2.lock().unwrap().lotate_rev();
                }
                _ => {}
            }

            match (*field_shared2.lock().unwrap()).get_event() {
                Some(field::Event::Gameover) => {
                    break;
                }
                None => {}
            }
        }
    });
    loop {
        thread::sleep(std::time::Duration::from_millis(DURATION.into()));
        {
            let mut field_locked = field_shared.lock().unwrap();
            (*field_locked).drop();
            field_locked.display(stdout);

            match field_locked.get_event() {
                Some(field::Event::Gameover) => {
                    break;
                }
                None => {}
            }
        }
    }

    handle_input.join().unwrap();
}