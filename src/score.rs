use termion::color;
use termion::input::MouseTerminal;
pub type StdoutType = MouseTerminal<termion::raw::RawTerminal<std::io::Stdout>>;
use std::io::Write;

use super::{FIELD_PADDING_X, FIELD_PADDING_Y, ROW_SIZE, SCORE_PADDING_Y};

#[derive(Debug, Clone, Default)]
pub struct Score {
    removed_line_count: u32,

    time: u32,
}

impl Score {
    pub fn display(&self, stdout: &mut StdoutType) {
        write!(
            stdout,
            "{}{}removed line number: {}",
            color::Fg(color::Red),
            termion::cursor::Goto(
                FIELD_PADDING_X,
                FIELD_PADDING_Y + (ROW_SIZE as u16) + SCORE_PADDING_Y
            ),
            self.removed_line_count
        )
        .expect("score's line_number can't display!");

        write!(
            stdout,
            "{}               time: {}",
            termion::cursor::Goto(
                FIELD_PADDING_X,
                1 + FIELD_PADDING_Y + (ROW_SIZE as u16) + SCORE_PADDING_Y
            ),
            self.time
        )
        .expect("score's time can't display!");
    }

    pub fn new() -> Score {
        Default::default()
    }

    pub fn spend_time(&mut self) {
        self.time += 1;
    }

    pub fn add_removed_line_count(&mut self, count: u32) {
        self.removed_line_count += count;
    }
}
