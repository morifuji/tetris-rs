use super::COLUMN_SIZE;
use super::FIELD_PADDING_X;
use super::FIELD_PADDING_Y;
use super::ROW_SIZE;
use termion::input::MouseTerminal;
pub type StdoutType = MouseTerminal<termion::raw::RawTerminal<std::io::Stdout>>;
use termion::color;

use super::mino::Mino;
use super::score::Score;
use std::io::Write;
use std::sync::{Arc, Mutex};

// type CellsType = [[Rc<RefCell<Mino>>; COLUMN_SIZE];ROW_SIZE];
type CellsType = [[bool; COLUMN_SIZE]; ROW_SIZE];

#[derive(Debug, Clone, Default)]
pub struct Field {
    cells: CellsType,

    dropping_mino: Arc<Mutex<Mino>>,

    mino_list: Vec<Mino>,

    mino_queue: Vec<Mino>,

    score: Score,
}

pub enum Event {
    Gameover,
}

impl Field {
    pub fn new() -> Field {
        // link: https://tyfkda.github.io/blog/2020/03/19/rust-init-array.html

        let first_mino = Mino::new();
        let first_mino_rc = Arc::new(Mutex::new(first_mino));

        Field {
            dropping_mino: Arc::clone(&first_mino_rc),
            mino_list: vec![],
            mino_queue: vec![
                Mino::new(),
                Mino::new(),
                Mino::new(),
                Mino::new(),
                Mino::new(),
            ],
            score: Score::new(),
            ..Default::default()
        }
    }

    fn sync_cells(&mut self) {
        // let mut cells: CellsType = Default::default();
        // for mino in self.mino_list.iter() {
        //     for position in mino.get_position_list() {
        //         cells[position.1][position.0] = true;
        //     }
        // }

        // self.cells = cells;
    }

    fn display_mino(&self, stdout: &mut StdoutType, mino: &Mino) {
        let color = mino.get_color();
        for position in mino.get_position_list() {
            write!(
                stdout,
                "{}{}■",
                color::Fg(&*color),
                termion::cursor::Goto(
                    position.0 as u16 + FIELD_PADDING_X,
                    position.1 as u16 + FIELD_PADDING_Y
                ),
            )
            .expect("mino_display error!");
        }
    }

    pub fn drop(&mut self) {
        self.score.spend_time();

        self.sync_cells();

        if self.is_collision(0, 1) {
            self.change_dropping_mino();
            return;
        }
        // for position in position_list {
        //     if (position.1 + 1) == ROW_SIZE || self.cells[position.1 + 1][position.0] {
        //         // Mino衝突
        //         self.change_dropping_mino();
        //         return;
        //     }
        // }

        (*self.dropping_mino.lock().unwrap()).drop();
    }

    fn is_collision(&mut self, x: isize, y: usize) -> bool {
        let position_list = (*self.dropping_mino.lock().unwrap()).get_position_list();
        for position in position_list {
            let next_x = x + (position.0 as isize);
            let next_y = position.1 + y;
            if next_y >= ROW_SIZE
                || next_x <= -1
                || next_x >= (COLUMN_SIZE as isize)
                || self.cells[next_y][next_x as usize]
            {
                return true;
            }
        }
        false
    }

    // mino: 衝突したMino
    fn change_dropping_mino(&mut self) {
        let mino_cloned = (*self.dropping_mino.lock().unwrap()).clone();

        for (x, y) in mino_cloned.get_position_list() {
            self.cells[y][x] = true
        }

        self.mino_list.push(mino_cloned);

        let next_mino = self.mino_queue.remove(0);
        self.dropping_mino = Arc::new(Mutex::new(next_mino.clone()));
        self.mino_queue.push(Mino::new());

        let delete_buried_line_count = self.delete_buried_line();
        self.score.add_removed_line_count(delete_buried_line_count);
    }

    fn delete_buried_line(&mut self) -> u32 {
        let mut line_count = 0;
        for i in (0..ROW_SIZE).rev() {
            if self.cells[i].iter().all(|v| *v) {
                line_count += 1;
                for j in (0..i + 1).rev() {
                    if j != 0 {
                        self.cells[j] = self.cells[j - 1];
                    } else {
                        self.cells[j] = Default::default();
                    }
                }
            }
        }

        line_count
    }

    fn display_minos(&mut self, stdout: &mut StdoutType) {
        for (y, x_list) in self.cells.iter().enumerate() {
            for (x, is_buried) in x_list.iter().enumerate() {
                if *is_buried {
                    write!(
                        stdout,
                        "{}{}■",
                        color::Fg(color::LightBlack),
                        termion::cursor::Goto(
                            FIELD_PADDING_X + x as u16,
                            FIELD_PADDING_Y + y as u16
                        ),
                    )
                    .expect("wall can't display!");
                }
            }
        }
        self.display_mino(stdout, &self.dropping_mino.lock().unwrap());
    }

    fn display_wall(&mut self, stdout: &mut StdoutType) {
        for i in 0..ROW_SIZE as u16 {
            write!(
                stdout,
                "{}{}■{}■",
                color::Fg(color::White),
                termion::cursor::Goto(FIELD_PADDING_X - 1, FIELD_PADDING_Y + i),
                termion::cursor::Goto(FIELD_PADDING_X + (COLUMN_SIZE as u16), FIELD_PADDING_Y + i),
            )
            .expect("wall can't display!");
        }

        write!(
            stdout,
            "{}{}{}",
            color::Fg(color::White),
            termion::cursor::Goto(FIELD_PADDING_X - 1, FIELD_PADDING_Y + (ROW_SIZE as u16)),
            "■".repeat(COLUMN_SIZE + 2),
        )
        .expect("wall can't display!");
    }

    pub fn get_event(&self) -> Option<Event> {
        // ゲームオーバー
        let position_list = (*self.dropping_mino.lock().unwrap()).get_position_list();
        for position in position_list {
            if self.cells[position.1][position.0] {
                return Some(Event::Gameover);
            }
        }

        return None;
    }

    pub fn display(&mut self, stdout: &mut StdoutType) {
        write!(stdout, "{}", termion::clear::All,).unwrap();
        self.display_wall(stdout);
        self.display_minos(stdout);

        self.display_mino_queue(stdout);
        self.score.display(stdout);
        stdout.flush().unwrap();
    }

    fn display_mino_queue(&mut self, stdout: &mut StdoutType) {
        for (i, mino) in self.mino_queue.iter().enumerate() {
            write!(
                stdout,
                "{}{}■",
                color::Fg(&*mino.get_color()),
                termion::cursor::Goto(
                    FIELD_PADDING_X * 2 + (COLUMN_SIZE as u16),
                    FIELD_PADDING_Y + ((ROW_SIZE.div_euclid(2) + i) as u16)
                ),
            )
            .expect("mino_queue can't display!");
        }
    }

    pub fn move_right(&mut self) {
        if self.is_collision(1, 0) {
            return;
        }
        self.dropping_mino.lock().unwrap().move_right();
    }

    pub fn move_left(&mut self) {
        if self.is_collision(-1, 0) {
            return;
        }
        self.dropping_mino.lock().unwrap().move_left();
    }

    pub fn move_down(&mut self) {
        self.dropping_mino.lock().unwrap().move_down();
    }

    pub fn force_drop(&mut self) {
        self.sync_cells();

        for i in 0..ROW_SIZE {
            if self.is_collision(0, i) {
                (*self.dropping_mino.lock().unwrap()).force_drop(i - 1);
                self.change_dropping_mino();
                return;
            }
        }

        panic!("force drop can't do!!");
    }

    pub fn lotate(&mut self) {
        (*self.dropping_mino.lock().unwrap()).lotate();
    }

    pub fn lotate_rev(&mut self) {
        (*self.dropping_mino.lock().unwrap()).lotate_rev();
    }
}

mod test {
    use super::*;

    const BOTTOM_ROW_INDEX: usize = ROW_SIZE - 1;

    #[test]
    fn test_change_dropping_mino() {
        let mut field = Field::new();

        assert_eq!(field.mino_queue.len(), 5);
        let second_mino = field.mino_queue[1].clone();
        field.change_dropping_mino();

        assert_eq!(field.mino_queue.len(), 5);
        assert_eq!(second_mino, field.mino_queue[0].clone());
    }

    #[test]
    fn test_buried_line() {
        let mut field = Field::new();

        field.cells[BOTTOM_ROW_INDEX] = [true; 10];
        field.cells[BOTTOM_ROW_INDEX - 1][1] = true;
        field.cells[BOTTOM_ROW_INDEX - 2] = [true; 10];

        field.delete_buried_line();

        assert_eq!(
            field.cells[BOTTOM_ROW_INDEX],
            [false, true, false, false, false, false, false, false, false, false,]
        );
        assert_eq!(field.cells[BOTTOM_ROW_INDEX - 1], [false; 10]);
        assert_eq!(field.cells[BOTTOM_ROW_INDEX - 2], [false; 10]);
    }

    #[test]
    fn test_is_collision() {
        let mut field = Field::new();

        field.cells[BOTTOM_ROW_INDEX] = [true; 10];
        field.cells[BOTTOM_ROW_INDEX - 1][1] = true;
        field.cells[BOTTOM_ROW_INDEX - 2] = [true; 10];

        field.delete_buried_line();

        assert_eq!(
            field.cells[BOTTOM_ROW_INDEX],
            [false, true, false, false, false, false, false, false, false, false,]
        );
        assert_eq!(field.cells[BOTTOM_ROW_INDEX - 1], [false; 10]);
        assert_eq!(field.cells[BOTTOM_ROW_INDEX - 2], [false; 10]);
    }
}
