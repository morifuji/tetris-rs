extern crate termion;
use std::io::Write;
use termion::color;

use super::COLUMN_SIZE;

use super::{StdoutType, FIELD_PADDING_X, FIELD_PADDING_Y};
use rand::Rng;

#[derive(Clone, Debug, Copy, std::cmp::PartialEq)]
pub enum MinoType {
    I,
    T,
    O,
    S,
    Z,
    L,
    J,
}

type Form = [[usize; 4]; 4];

#[derive(Clone, Debug, Copy, std::cmp::PartialEq)]
pub enum MinoDirection {
    // 0
    Zero,
    // 90 degree
    OneQuater,
    // 180 degree
    Half,
    // 270 degree
    ThreeFourths,
}

#[derive(Clone, Debug)]
pub struct Mino {
    pub mino_type: MinoType,
    pub mino_direction: MinoDirection,

    // マイナスもありえる
    // FIXME usizeに
    pub x: isize,
    pub y: usize,
}

impl std::cmp::PartialEq for Mino {
    fn eq(&self, other: &Mino) -> bool {
        // TODO UUID付与して比較
        self.mino_type == other.mino_type && self.mino_direction == other.mino_direction
    }
}
impl Default for Mino {
    fn default() -> Self {
        Mino::new_by_type(MinoType::I)
    }
}

impl Mino {
    pub fn new_by_type(_type: MinoType) -> Mino {
        Mino {
            mino_type: _type,
            mino_direction: MinoDirection::Zero,
            x: COLUMN_SIZE.div_euclid(2) as isize,
            y: 0,
        }
    }

    pub fn new() -> Mino {
        let mino_type = Mino::get_random_mino_type();

        Mino {
            mino_type: mino_type,
            mino_direction: MinoDirection::Zero,
            x: COLUMN_SIZE.div_euclid(2) as isize,
            y: 0,
        }
    }

    fn get_random_mino_type() -> MinoType {
        let mut rng = rand::thread_rng();
        let x = rng.gen_range(0, 7);
        let mino_type = vec![
            MinoType::I,
            MinoType::T,
            MinoType::O,
            MinoType::S,
            MinoType::Z,
            MinoType::L,
            MinoType::J,
        ][x];

        mino_type
    }

    pub fn lotate(&mut self) {
        let next_direction = match self.mino_direction {
            MinoDirection::Zero => MinoDirection::OneQuater,
            MinoDirection::OneQuater => MinoDirection::Half,
            MinoDirection::Half => MinoDirection::ThreeFourths,
            MinoDirection::ThreeFourths => MinoDirection::Zero,
        };

        self.mino_direction = next_direction;
    }

    pub fn lotate_rev(&mut self) {
        let next_direction = match self.mino_direction {
            MinoDirection::OneQuater => MinoDirection::Zero,
            MinoDirection::Half => MinoDirection::OneQuater,
            MinoDirection::ThreeFourths => MinoDirection::Half,
            MinoDirection::Zero => MinoDirection::ThreeFourths,
        };

        self.mino_direction = next_direction;
    }

    pub fn get_position_list(&self) -> Vec<(usize, usize)> {
        let form = self.get_form();

        let mut position_list: Vec<(usize, usize)> = vec![];
        for y in 0..4 {
            for x in 0..4 {
                if form[y][x] == 1 {
                    position_list.push(((x as isize + self.x) as usize, y + self.y));
                }
            }
        }

        position_list
    }

    fn get_form(&self) -> Form {
        match self.mino_type.clone() {
            MinoType::I => match self.mino_direction {
                MinoDirection::Zero => [[0, 1, 0, 0], [0, 1, 0, 0], [0, 1, 0, 0], [0, 1, 0, 0]],
                MinoDirection::OneQuater => {
                    [[0, 0, 0, 0], [1, 1, 1, 1], [0, 0, 0, 0], [0, 0, 0, 0]]
                }
                MinoDirection::Half => [[0, 1, 0, 0], [0, 1, 0, 0], [0, 1, 0, 0], [0, 1, 0, 0]],
                MinoDirection::ThreeFourths => {
                    [[0, 0, 0, 0], [1, 1, 1, 1], [0, 0, 0, 0], [0, 0, 0, 0]]
                }
            },
            MinoType::T => match self.mino_direction {
                MinoDirection::Zero => [[0, 0, 0, 0], [1, 1, 1, 0], [0, 1, 0, 0], [0, 0, 0, 0]],
                MinoDirection::OneQuater => {
                    [[0, 1, 0, 0], [1, 1, 0, 0], [0, 1, 0, 0], [0, 0, 0, 0]]
                }
                MinoDirection::Half => [[0, 1, 0, 0], [1, 1, 1, 0], [0, 0, 0, 0], [0, 0, 0, 0]],
                MinoDirection::ThreeFourths => {
                    [[0, 1, 0, 0], [0, 1, 1, 0], [0, 1, 0, 0], [0, 0, 0, 0]]
                }
            },
            MinoType::O => [[0, 0, 0, 0], [0, 1, 1, 0], [0, 1, 1, 0], [0, 0, 0, 0]],
            MinoType::S => match self.mino_direction {
                MinoDirection::Zero => [[0, 0, 0, 0], [0, 1, 1, 0], [1, 1, 0, 0], [0, 0, 0, 0]],
                MinoDirection::OneQuater => {
                    [[1, 0, 0, 0], [1, 1, 0, 0], [0, 1, 0, 0], [0, 0, 0, 0]]
                }
                MinoDirection::Half => [[0, 0, 0, 0], [0, 1, 1, 0], [1, 1, 0, 0], [0, 0, 0, 0]],
                MinoDirection::ThreeFourths => {
                    [[1, 0, 0, 0], [1, 1, 0, 0], [0, 1, 0, 0], [0, 0, 0, 0]]
                }
            },
            MinoType::Z => match self.mino_direction {
                MinoDirection::Zero => [[0, 0, 0, 0], [1, 1, 0, 0], [0, 1, 1, 0], [0, 0, 0, 0]],
                MinoDirection::OneQuater => {
                    [[0, 1, 0, 0], [1, 1, 0, 0], [1, 0, 0, 0], [0, 0, 0, 0]]
                }
                MinoDirection::Half => [[0, 0, 0, 0], [1, 1, 0, 0], [0, 1, 1, 0], [0, 0, 0, 0]],
                MinoDirection::ThreeFourths => {
                    [[0, 1, 0, 0], [1, 1, 0, 0], [1, 0, 0, 0], [0, 0, 0, 0]]
                }
            },
            MinoType::L => match self.mino_direction {
                MinoDirection::Zero => [[0, 1, 0, 0], [0, 1, 0, 0], [0, 1, 1, 0], [0, 0, 0, 0]],
                MinoDirection::OneQuater => {
                    [[0, 0, 0, 0], [1, 1, 1, 0], [1, 0, 0, 0], [0, 0, 0, 0]]
                }
                MinoDirection::Half => [[1, 1, 0, 0], [0, 1, 0, 0], [0, 1, 0, 0], [0, 0, 0, 0]],
                MinoDirection::ThreeFourths => {
                    [[0, 0, 1, 0], [1, 1, 1, 0], [0, 0, 0, 0], [0, 0, 0, 0]]
                }
            },
            MinoType::J => match self.mino_direction {
                MinoDirection::Zero => [[0, 1, 0, 0], [0, 1, 0, 0], [1, 1, 0, 0], [0, 0, 0, 0]],
                MinoDirection::OneQuater => {
                    [[1, 0, 0, 0], [1, 1, 1, 0], [0, 0, 0, 0], [0, 0, 0, 0]]
                }
                MinoDirection::Half => [[0, 1, 1, 0], [0, 1, 0, 0], [0, 1, 0, 0], [0, 0, 0, 0]],
                MinoDirection::ThreeFourths => {
                    [[0, 0, 0, 0], [1, 1, 1, 0], [0, 0, 1, 0], [0, 0, 0, 0]]
                }
            },
        }
    }

    pub fn get_color(&self) -> Box<dyn color::Color> {
        match self.mino_type {
            MinoType::I => Box::new(color::Red),
            MinoType::O => Box::new(color::Blue),
            MinoType::T => Box::new(color::Yellow),
            MinoType::S => Box::new(color::Green),
            MinoType::Z => Box::new(color::LightMagenta),
            MinoType::J => Box::new(color::LightCyan),
            MinoType::L => Box::new(color::Magenta),
        }
    }

    // fn display(&self, stdout: &mut StdoutType) {
    //     let offset_x = FIELD_PADDING_X + (self.x as u16);
    //     let offset_y = FIELD_PADDING_Y + (self.y as u16);
    //     let color_box = self.get_color();
    //     let color = color::Fg(&*color_box);
    //     let form = self.get_form();
    //     write!(stdout, "{}{}", termion::clear::All, color).expect("mino_display error!");
    //     for y in 0..4 {
    //         write!(
    //             stdout,
    //             "{}",
    //             termion::cursor::Goto((offset_x) as u16, (offset_y + y) as u16),
    //         )
    //         .expect("想定外");
    //         for x in 0..4 {
    //             if form[y as usize][x as usize] == 1 {
    //                 write!(
    //                     stdout,
    //                     "{}{}",
    //                     termion::cursor::Goto((offset_x + x) as u16, (offset_y + y) as u16),
    //                     "■"
    //                 )
    //                 .expect("想定外");
    //             }
    //         }
    //     }
    //     stdout.flush().unwrap();
    // }

    pub fn drop(&mut self) {
        self.y += 1;
    }

    pub fn move_right(&mut self) {
        self.x += 1;
    }

    pub fn move_left(&mut self) {
        self.x -= 1;
    }

    pub fn move_down(&mut self) {
        self.y += 1;
    }

    pub fn force_drop(&mut self, y: usize) {
        self.y += y;
    }
}
mod test {
    use super::*;
    use crate::termion::raw::IntoRawMode;
    use std::io::{stdin, stdout};
    use termion::event::{Event, Key, MouseEvent};
    use termion::input::{MouseTerminal, TermRead};
    use termion::style;

    #[test]
    fn test_get_position_list_t() {
        let mino = Mino::new_by_type(MinoType::T);
        let default_x = COLUMN_SIZE.div_euclid(2);

        assert_eq!(mino.get_position_list().len(), 4);
        assert_eq!(
            mino.get_position_list(),
            vec!((default_x, 1), (default_x + 1, 1), (default_x + 2, 1), (default_x + 1, 2))
        );
    }

    #[test]
    fn test_get_position_list_s() {
        let mino = Mino::new_by_type(MinoType::S);
        let default_x = COLUMN_SIZE.div_euclid(2);

        assert_eq!(mino.get_position_list().len(), 4);
        assert_eq!(
            mino.get_position_list(),
            vec!((default_x + 1, 1), (default_x + 2, 1), (default_x, 2), (default_x + 1, 2))
        );
    }

    #[test]
    fn test_get_position_list_o() {
        let mino = Mino::new_by_type(MinoType::O);
        let default_x = COLUMN_SIZE.div_euclid(2);

        assert_eq!(mino.get_position_list().len(), 4);
        assert_eq!(
            mino.get_position_list(),
            vec!((default_x +1, 1), (default_x + 2, 1), (default_x + 1, 2), (default_x + 2, 2))
        );
    }
}
